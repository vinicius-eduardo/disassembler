/***********************************************************************#
#		EXERCÍCIO COMPUTACIONAL 2 - ARQUITETURA DE COMPUTADORES			#
#	Discente: Vinicius Eduardo Pinheiro Evangelista						#
#	Docente: Jose Everardo Bessa Maia 									#
#																		#
#					*Mips disassembler*									#
*************************************************************************/
#include<iostream>
#include<fstream>
#include<vector>
#include<string>

using namespace std;

typedef struct LabelAddresses{
	int index;
	string lName;
}lAddr;

typedef struct preFile{
	vector<string> lines;
	vector<lAddr> labelsAddr;
	int instructionCount = 0;
}pFile;

void printFile(pFile output){
	ofstream outFile("saida.txt");

	for(int i = 0; i < output.lines.size(); i++){

		if(output.labelsAddr.size() != 0){	//search for labels
			for(int j = 0; j < output.labelsAddr.size(); j++){
				
				if(output.labelsAddr[j].index == i)
					outFile << output.labelsAddr[j].lName << ":\t";
				
				if(output.labelsAddr[j].index >= output.lines.size())	//search for labels at the end of the file
					output.labelsAddr[j].index = -1;
			
			}
		}

		outFile << output.lines[i];
	}

	for(int i = 0; i < output.labelsAddr.size(); i++){	// print labels that leads to no instructions (end of the file)
		if(output.labelsAddr[i].index == -1){
			outFile << output.labelsAddr[i].lName << ":" << endl;
		}
	}

}

string makeLabel(pFile &output, int cont){
	string temp = "Label" + to_string(cont);
	lAddr temp2;
	
	temp2.lName = temp;
	temp2.index = cont;

	if(output.labelsAddr.size() == 0){
		
		output.labelsAddr.push_back(temp2);

	}else{
		for(int i = 0; i < output.labelsAddr.size(); i++){
			
			if(i+1 == output.labelsAddr.size() && temp != output.labelsAddr[i].lName){
				output.labelsAddr.push_back(temp2);
			}

		}
	}

	return temp;
}

int binToDecU(const string binValue){	//unsigned
	int sum = 0, cont = 1;
	int bitsize = binValue.size()-1;
	
	for(int i = bitsize; i >= 0; i--){
		
		if(i == bitsize && binValue[bitsize] == '1'){	
			sum = cont;
		}
		else if(binValue[i] == '1'){	
			sum += cont;
		}
		cont *= 2;

	}
	return sum;
}

int binToDecS(const string binValue){	//signed
	
	if(binValue[0] == '1'){
		string sTemp = binValue;
		int sum, cont = -1;

		for(int i = 1; i < binValue.size(); i++){
			cont *= 2;
		}

		sTemp.erase(sTemp.begin());
		sum = binToDecU(sTemp);
		sum += cont;

		return sum;
	}else{
		return binToDecU(binValue);
	}

}

string getField(const string address, const string str){
	string temp;
	int start, end;

	if(str == "op"){
		start = 0;
		end = 5;
	}else if(str == "rs"){
		start = 6;
		end = 10;
	}else if(str == "rt"){
		start = 11;
		end = 15;
	}else if(str == "rd"){
		start = 16;
		end = 20;
	}else if(str == "sa"){
		start = 22;
		end = 25;
	}else if(str == "func"){
		start = 26;
		end = 31;
	}else if(str == "imm"){
		start = 16;
		end = 31;
	}else if(str == "target"){
		start = 6;
		end = 31;
	}else if(str == "rdsa"){
		start = 16;
		end = 25;
	}
	else if(str == "rsrt"){
		start = 6;
		end = 15;
	}
	else if(str == "rtrdsa"){
		start = 11;
		end = 25;
	}else if(str == "break"){
		start = 6;
		end = 25;
	}

	for(int i = start; i <= end; i++)
		temp += address[i];

	return temp;
}

string regName(const string &str){
	switch( stoi(str) ){
		case 0:	return "$zero";
		case 1:	return "$at";
		case 10:	return "$v0";
		case 11:	return "$v1";
		case 100:	return "$a0";
		case 101:	return "$a1";
		case 110:	return "$a2";
		case 111:	return "$a3";
		case 1000:	return "$t0";
		case 1001:	return "$t1";
		case 1010:	return "$t2";
		case 1011:	return "$t3";
		case 1100:	return "$t4";
		case 1101:	return "$t5";
		case 1110:	return "$t6";
		case 1111:	return "$t7";
		case 10000:	return "$s0";
		case 10001:	return "$s1";
		case 10010:	return "$s2";
		case 10011:	return "$s3";
		case 10100:	return "$s4";
		case 10101:	return "$s5";
		case 10110:	return "$s6";
		case 10111:	return "$s7";
		case 11000:	return "$t8";
		case 11001:	return "$t9";
		case 11010:	return "$k0";
		case 11011:	return "$k1";
		case 11100:	return "$gp";
		case 11101:	return "$sp";
		case 11110:	return "$fp";
		case 11111:	return "$ra";
		default:
			cout << "*register not found*" << endl;
		break;
	}
}

void rType(const string &address, pFile &output){
	string rs, rt, rd, sa, func, temp;
	rs = getField(address, "rs");
	rt = getField(address, "rt");
	rd = getField(address, "rd");
	sa = getField(address, "sa");
	func = getField(address, "func");

	switch( stoi(func) ){
		case 100000:	//add*
			temp = "add\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100001:	//addu*
			temp = "addu\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100100:	//and*
			temp = "and\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 11010:	//div*
			temp = "div\t" + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 11011:	//divu*
			temp = "divu\t" + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 1000:	//jr*
			temp = "jr\t" + regName(rs) + "\n";
		break;
		case 1001:	//jalr*
			temp = "jalr\t" + regName(rs) + "\n";
		break;
		case 10000:	//mfhi*
			temp = "mfhi\t" + regName(rd) + "\n";
		break;
		case 10010:	//mflo*
			temp = "mflo\t" + regName(rd) + "\n";
		break;
		case 11000:	//mult*
			temp = "mult\t" + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 11001:	//multu*
			temp = "multu\t" + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100111:	//nor*
			temp = "nor\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100110:	//xor*
			temp = "xor\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100101:	//or*
			temp = "or\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 101010:	//slt*
			temp = "slt\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 101011:	//sltu*
			temp = "sltu\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 0:	//sll*
			temp = "sll\t" + regName(rd) + ", " + regName(rt) + ", " + to_string(binToDecU(sa)) + "\n";
		break;
		case 10:	//srl*
			temp = "srl\t" + regName(rd) + ", " + regName(rt) + ", " + to_string(binToDecU(sa)) + "\n";
		break;
		case 11:	//sra*
			temp = "sra\t" + regName(rd) + ", " + regName(rt) + ", " + to_string(binToDecU(sa)) + "\n";
		break;
		case 100010:	//sub*
			temp = "sub\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100011:	//subu*
			temp = "subu\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		case 100:	//sllv*
			temp = "sllv\t" + regName(rd) + ", " + regName(rt) + ", " + regName(rs) + "\n";
		break;
		case 110:	//srlv*
			temp = "srlv\t" + regName(rd) + ", " + regName(rt) + ", " + regName(rs) + "\n";
		break;
		case 111:	//srav*
			temp = "srav\t" + regName(rd) + ", " + regName(rt) + ", " + regName(rs) + "\n";
		break;
		case 1100:	//syscall*
			temp = "syscall\n";
		break;
		default:
			cout << "*R-type sub-instruction not found*" << endl;
			return;
		break;
	}

	output.lines.push_back(temp);
	output.instructionCount++;

}

void iType(const string &address, pFile &output){
	string op, rs, rt, offImm, temp;
	op = getField(address, "op");
	rs = getField(address, "rs");
	rt = getField(address, "rt");
	offImm = getField(address, "imm");
	int cont;

	switch( stoi(op) ){
		case 1000:	//addi*
			temp = "addi\t" + regName(rt) + ", " + regName(rs) + ", " + to_string(binToDecS(offImm)) + "\n";
		break;
		case 1001:	//addiu*
			temp = "addiu\t" + regName(rt) + ", " + regName(rs) + ", " + to_string(binToDecU(offImm)) + "\n";
		break;
		case 1100:	//andi*
			temp = "andi\t" + regName(rt) + ", " + regName(rs) + ", " + offImm + "\n";
		break;
		case 1111:	//lui*
			temp = "lui\t" + regName(rt) + ", " + offImm + "\n";
		break;
		case 1101:	//ori*
			temp = "ori\t" + regName(rt) + ", " + regName(rs) + ", " + offImm + "\n";
		break;
		case 1010:	//slti*
			temp = "slti\t" + regName(rt) + ", " + regName(rs) + ", " + to_string(binToDecS(offImm)) + "\n";
		break;
		case 1011:	//sltiu*
			temp = "sltiu\t" + regName(rt) + ", " + regName(rs) + ", " + to_string(binToDecU(offImm)) + "\n";
		break;
		case 1110:	//xori*
			temp = "xori\t" + regName(rt) + ", " + regName(rs) + ", " + offImm + "\n";
		break;
		case 100:	//beq-
			cont = output.instructionCount + (binToDecS(offImm)+1);
			temp = "beq\t" + regName(rs) + ", " + regName(rt) + ", " + makeLabel(output, cont) + "\n";
		break;
		case 101:	//bne-
			cont = output.instructionCount + (binToDecS(offImm)+1);
			temp = "bne\t" + regName(rs) + ", " + regName(rt) + ", " + makeLabel(output, cont) + "\n";
		break;
		case 100000:	//lb*
			temp = "lb\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 100100:	//lbu*
			temp = "lbu\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 100001:	//lh*
			temp = "lh\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 100101:	//lhu*
			temp = "lhu\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 100011:	//lw*
			temp = "lw\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 101000:	//sb*
			temp = "sb\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 101001:	//sh*
			temp = "sh\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 101011:	//sw*
			temp = "sw\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 110000:	//ll*
			temp = "ll\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		case 111000:	//sc*
			temp = "sc\t" + regName(rt) + ", " + to_string(binToDecS(offImm)) + "(" + regName(rs) + ")" + "\n";
		break;
		default:
			cout << "*R-type sub-instruction not found*" << endl;
			return;
		break;
	}

	output.lines.push_back(temp);
	output.instructionCount++;

}

void jType(const string &address, pFile &output){
	string op, offImm, target, temp;
	op = getField(address, "op");
	offImm = getField(address, "imm");
	target = getField(address, "target");
	int cont;

	switch( stoi(op) ){
		case 10:	//jump
			cont = binToDecS(offImm);
			temp = "j\t" + makeLabel(output, cont) + "\n";
		break;
		case 11:	//jal
			cont = binToDecS(offImm);
			temp = "jal\t" + makeLabel(output, cont) + "\n";
		break;
		default:
			cout << "*R-type sub-instruction not found*" << endl;
			return;
		break;
	}

	output.lines.push_back(temp);
	output.instructionCount++;

}

void pseudoType(const string &address, pFile output){
	string rs, rt, rd, sa, func, temp;
	rs = getField(address, "rs");
	rt = getField(address, "rt");
	rd = getField(address, "rd");
	sa = getField(address, "sa");
	func = getField(address, "func");
	
	switch( stoi(func) ){
		case 10:	//mul
			temp = "mul\t" + regName(rd) + ", " + regName(rs) + ", " + regName(rt) + "\n";
		break;
		default:
			cout << "*Pseudo-type sub-instruction not found*" << endl;
			
			return;
		break;
	}

	output.lines.push_back(temp);
	output.instructionCount++;

}

void getType(const string &address, pFile &output){

	switch( stoi(getField(address, "op")) ){
		case 0:
			rType(address, output);
		break;
		case 1000:
		case 1001:
		case 1100:
		case 1111:
		case 1101:
		case 1010:
		case 1011:
		case 1110:
		case 100:
		case 101:
		case 100000:
		case 100100:
		case 100001:
		case 100101:
		case 100011:
		case 101000:
		case 101001:
		case 101011:
		case 110000:
		case 111000:
			iType(address, output);
		break;
		case 10:
		case 11:
			jType(address, output);
		break;
		case 11100:	//mul #pseudo instruction
			pseudoType(address, output);
		break;
		default:
			cout << "*type not found*" << endl;
		break;
	}
}

int main(){
	ifstream inFile("entrada.txt");
	pFile output;
	string line;

	if(inFile){

		while(getline(inFile, line)){
			
			getType(line, output);
		
		}

	printFile(output);

	inFile.close();
	
	}else{
		cout << "*No in file to read*" << endl;
	}
	
	return 0;
}