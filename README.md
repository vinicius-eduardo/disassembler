Disassembler binario -> MIPS

Entrada -> arquivo binario com linhas de 32 bits
Saida -> arquivo .txt com instrucoes MIPS

O programa MARS foi utilizado para obter o arquivo de entrada em binario.
Dispoivel em: (http://courses.missouristate.edu/kenvollmar/mars/)

O nome do arquivo de entrada e saida pode ser alterado em inFile(entrada) e outFile(saida).